<%@ page import="java.util.*" %>
<%@ page import="Country.models.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>
<%
	//달력 인스턴스를 가져온다.
	Calendar cal = Calendar.getInstance();
	int nowYear = cal.get(Calendar.YEAR);
 %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TOURMATE</title>

<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script src="js/jquery-2.1.1.js"></script>
<script src="js/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
<!--
  $(function() {
    $( "#datepicker" ).datepicker({
    	changeMonth: true, // 콤보박스에 월 보이기
    	changeYear:  true //콤보박스에 년도 보이기
    	
    });
    $.datepicker.setDefaults({dateFormat:'yyyy-mm-dd'});
  });
  
 //-->
</script>

</head>
<script  src="js/check.js" charset="euc-kr"> </script>

<body>
<%
	CountryDAO dao = new CountryDAO();
	ArrayList<CountryDTO> list = dao.selectCountry();
%>
<H1>회원가입</H1>

<form name="join" action="MemberProcess" method="post">
<table id="membertable">
	<tr>
		<td><input type="text" name="m_name" required="required"placeholder="이름"/></td>
	</tr>
	<tr>
		<td><input type="text" name="m_email" required="required" placeholder="Email"/></td>
	</tr>
	<tr>
		<td><input type="password" name="m_passwd" maxlength="15" required="required" placeholder="비밀번호 (6~15)" /></td>
	</tr>
	<tr>
		<td><input type="password" name="c_passwd" maxlength="15" required="required" placeholder="비밀번호 확인" /></td>
	</tr>
	<tr>
	 <!-- 국가 SelectBox -->
	 	<td><select name="m_country" required="required" >
			<option>국가</option>
			<%
				if(list.size()>0){
				for(int i=0; i<list.size(); i++){
					CountryDTO dto = list.get(i);
			%>
			<option value=<%= dto.getEng() %>>
			<%= dto.getEng() %>
			</option>
			<%
				}
				}
			%>
			</select>
		</td>
	</tr>
	<tr>
		<td><input type="text" id="datepicker" name="m_birth" placeholder="생년월일"></td>
		
	</tr>
	<tr>
		<td><input type="radio" id="female" name="m_gender" value="female" /><label for="female">여성</label>
		&nbsp;<input type="radio" id="male" name="m_gender"  value="male" /><label for="male">남성</label>
		</td>
	</tr>
	<tr>
		<td><input type="submit" value="OK"/></td>
	</tr>

</table>
</form>
</body>
</html>

