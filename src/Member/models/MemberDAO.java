package Member.models;
import java.sql.*;
import javax.servlet.ServletException;

public class MemberDAO {
	private Connection conn;
	public MemberDAO() throws ServletException{
		try {
			Class.forName("org.gjt.mm.mysql.Driver");
		} catch(ClassNotFoundException ex){
			throw new ServletException("드라이버 오류!");
		}
		String url="jdbc:mysql://localhost:3306/tourmate";
		String id  = "root";
		String pw = "sang0916";
		try{
			conn = DriverManager.getConnection(url, id, pw);
		} catch(SQLException ex) {
			throw new ServletException("접속 오류!");
		}
	}
	
	public boolean registerMember (MemberDTO dto) throws ServletException {
		String query = "insert into member values (null,?,?,?,?,?,?)";
		try{
			PreparedStatement pstmt = conn.prepareStatement(query);
			pstmt.setString(1, dto.getName());
			pstmt.setString(2, dto.getEmail());
			pstmt.setString(3, dto.getPasswd());
			pstmt.setString(4, dto.getCountry());
			pstmt.setString(5, dto.getGender());
			pstmt.setString(6, dto.getBirth());
			
			pstmt.executeUpdate();
			pstmt.close();
		} catch(SQLException ex) {
			throw new ServletException("등록 실패");
			
		} finally {
			this.close();
		}
		return true;
	}
	
	
	private void close(){
		try {
			if(conn != null && !conn.isClosed()) conn.close();
		} catch(SQLException ex) { conn = null;}
	}

}
