package Member.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Member.models.*;


public class MemberProcess extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("m_name");
		String email = request.getParameter("m_email");
		String passwd = request.getParameter("m_passwd");
		String country = request.getParameter("m_country");
		String birth = request.getParameter("m_birth");
		String gender = request.getParameter("m_gender");
		
		MemberDTO dto = new MemberDTO();
		dto.setName(name);
		dto.setEmail(email);
		dto.setPasswd(passwd);
		dto.setCountry(country);
		dto.setBirth(birth);
		dto.setGender(gender);
		
		MemberDAO dao = new MemberDAO();
		
		boolean bool = dao.registerMember(dto);
		if(bool){
			response.sendRedirect("Login.jsp");
			}
		
		
	}

}
