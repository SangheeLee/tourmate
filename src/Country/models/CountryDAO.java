package Country.models;
import java.sql.*;
import java.util.ArrayList;

import javax.servlet.ServletException;

public class CountryDAO {
	private Connection conn;
	public CountryDAO() throws ServletException{
		try {
			Class.forName("org.gjt.mm.mysql.Driver");
		} catch(ClassNotFoundException ex){
			throw new ServletException("드라이버 오류!");
		}
		
		String url="jdbc:mysql://localhost:3306/tourmate";
		String id  = "root";
		String pw = "sang0916";
		try{
			conn = DriverManager.getConnection(url, id, pw);
		} catch(SQLException ex) {
			throw new ServletException("접속 오류!");
		}
	}
	
	public ArrayList<CountryDTO> selectCountry() throws ServletException {
		ArrayList<CountryDTO> list = new ArrayList<CountryDTO>();
		String query = "select * from country";
		try{
			PreparedStatement pstmt = conn.prepareStatement(query);
			ResultSet rs = pstmt.executeQuery();
			
			while(rs.next()){
				CountryDTO dto = new CountryDTO ();
				int codeNum = rs.getInt("code_num");
				String codeTwo = rs.getString("code_two");
				String codeThree = rs.getString("code_three");
				String nameEng = rs.getString("name_eng");
				String nameKr = rs.getString("name_kr");
				
				dto.setNum(codeNum); 
				dto.setCode2(codeTwo);
				dto.setCode3(codeThree);
				dto.setEng(nameEng);
				dto.setKr(nameKr);
				
				list.add(dto);				
			}
			pstmt.close();
		} catch (SQLException ex) {
			throw new ServletException("Fail!!");			
		} finally{
			this.close();
		}
		return list;
		
	}
	private void close(){
		try {
			if(conn != null && !conn.isClosed()) conn.close();
		} catch(SQLException ex) { conn = null;}
	}
	
}
